package com.example.filereader

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_keluar.*
import org.json.JSONArray
import org.json.JSONObject

class KeluarActivity : AppCompatActivity() {
    val url = "http://192.168.1.12/web/X.PESAN/20200821_LIYUS/aplikasi-surat/show_data.php"
    lateinit var KeluarAdapter : AdapterDataKeluar
    var daftarKeluar = mutableListOf<HashMap<String,String>>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_keluar)
        KeluarAdapter = AdapterDataKeluar(daftarKeluar)
        lsKeluar.layoutManager = LinearLayoutManager(this)
        lsKeluar.adapter = KeluarAdapter
    }

    override fun onStart() {
        super.onStart()
        showdataabsen()
    }

    fun showdataabsen(){
//        Toast.makeText(this,"masuk show data", Toast.LENGTH_SHORT).show()
        val request = object : StringRequest(
            Request.Method.POST,url,Response.Listener { response ->
//                Toast.makeText(this,"masuk respon", Toast.LENGTH_SHORT).show()
                daftarKeluar.clear()
                val jsonArray = JSONArray(response)
                for (x in 0.. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var k = HashMap<String,String>()
                    k.put("id_surat_keluar",jsonObject.getString("id_surat_keluar"))
                    k.put("no_surat",jsonObject.getString("no_surat"))
                    k.put("perihal",jsonObject.getString("perihal"))
                    k.put("tgl_kirim",jsonObject.getString("tgl_kirim"))
                    daftarKeluar.add(k)
                }
                KeluarAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"gagal memuat data !!!", Toast.LENGTH_SHORT).show()
            }
        ){}
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}