package com.example.filereader

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnKeluar.setOnClickListener(this)
        btnMasuk.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMasuk->{

            }
            R.id.btnKeluar->{
                val intent = Intent(this, KeluarActivity::class.java)
                startActivity(intent)
            }
        }
    }
}