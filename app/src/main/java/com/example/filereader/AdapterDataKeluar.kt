package com.example.filereader

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class AdapterDataKeluar(val datakeluar : List<HashMap<String,String>>) : RecyclerView.Adapter<AdapterDataKeluar.HolderDataKeluar>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDataKeluar.HolderDataKeluar {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_keluar,parent,false)
        return HolderDataKeluar(v)
    }

    override fun onBindViewHolder(holder: AdapterDataKeluar.HolderDataKeluar, position: Int) {
        val data = datakeluar.get(position)
        holder.txtSurat.setText(data.get(("id_surat_keluar")))
        holder.txtNomor.setText(data.get(("no_surat")))
        holder.txtPerihal.setText(data.get(("perihal")))
        holder.txtTglKirim.setText(data.get(("tgl_kirim")))
    }

    override fun getItemCount(): Int {
        return datakeluar.size
    }

    inner class HolderDataKeluar(v : View): RecyclerView.ViewHolder(v){
        val txtSurat = v.findViewById<TextView>(R.id.txSurat)
        val txtNomor = v.findViewById<TextView>(R.id.txNomor)
        val txtPerihal = v.findViewById<TextView>(R.id.txPerihal)
        val txtTglKirim = v.findViewById<TextView>(R.id.txTglKirim)
        val CLayout = v.findViewById<ConstraintLayout>(R.id.CLayout)
    }
}